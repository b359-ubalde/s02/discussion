# Inputs in Python
# input() is similar to prompt in JavaScript that seeks to gather data from the user input
username = input("please enter your name: \n")
print(f"Hi, {username}! Welcome to Python Short Course")
num1 = int(input("Enter first number: \n"))
num2 = int(input("Enter second number: \n"))
print(f"The sum of num1 and num2 is {num1 + num2}")

# You can check the type of a variable by using type(). It is similar to tpyeof() fromm JS
print(type(num1))
print(type(num2))


# Control Structures:
# 1. Selection
# - Allows the program to choose among the choices and run specific codes depending on the choice taken.
# 2. Repetition
# - Allows the program to repeat certain blocks of code given a starting condition and terminating condition.

# If-Else Statements (Selection Structure)
test_num = 75
if test_num >= 60:
    print("Test passed")
else:
    print("Test failed")


# If-Else Chains
test_num2 = int(input("Please enter 2nd number: \n"))

if test_num2 > 0:
    print("The number is positive")
# elif is the shorthand of Else If
elif test_num2 == 0:
    print("Number is 0")
else:
    print("The number is negative")


# Mini Exercise: Create an If Else Statement that determines is a number is divisible by 3, 5, or both
test_num3 = int(input("Enter a number: "))

if test_num3 % 3 == 0 and test_num3 % 5 == 0:
    print("The number is divisible by both 5 and 3")
elif test_num3 % 5 == 0:
    print("The number is divisible by 5")
elif test_num3 % 3 == 0:
    print("The number is divisible by 3")
else:
    print("The number is not divisible by 5 nor 3")


# Loops (Repetition Controol Structure)

# While Loops are used to execute a set of statements as long as the condition is true
i = 1
while i <= 5:
    print(f"The current count: {i}")
    i += 1


# For loops are used for iterating over a sequence
# "fruits" are called list in Python, and not array
fruits = ["apple", "banana", "cherry"]

for indiv_fruit in fruits:
    print(indiv_fruit)


# range() method
# Syntax:
# range(stop)
# range(start, stop)
# range(start, stop, step)
for x in range(6):
    print(f"The current value is {x}")
# The range() function defaults to 0 as a starting value. AS such, this prints the values from 0 to 5.

for x in range(6, 10):
    print(f"The current value is: {x}")
# Print the value from 6 to 9. Always remember that the range always prints to n-1

for x in range(6, 20, 2):
    print(f"The current value is: {x}")
# A third argument can be added to specify the increment of the loop.

# Mini Exercise
# Create a loop to count and display the number of even numbers between 1 and 20.
# Print "The number of even numbers between 1 and 20 is: <a total number of even numbers>"
count = 0
for x in range(1, 21):
    if num % 2 == 0:
        count += 1
print(f"The number of even numbers between 1 and 20 is: {count}")


# Mini Exercise:
# Write a program that will display the multiplication table for that number
test_num = int(input("Enter a number: "))

for x in range(1, 11):
    print(f"{test_num} x {x} = {test_num*x}")


# Break Statement - used for breaking a loop
j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j += 1


# Continue Statement - returns the control to the beginning of the while loop and continue with the next iteration
k = 1
while k < 6:
    k += 1  # 1 is not printed since 1 is turned into 2 here
    if k == 3:
        continue
    print(k)
